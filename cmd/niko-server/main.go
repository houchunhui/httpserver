package main

import (
	"flag"
	"fmt"
	"github.com/golang/glog"
	"gitlab.com/houchunhui/httpserver/v1/internal/service"
	niko "gitlab.com/houchunhui/httpserver/v1/pkg/server"
)

//用于指定多网卡的某一ip
var ip string

//用于指定服务的端口
var port int

func init() {
	flag.StringVar(&ip, "ip", "localhost", "set the server ip adder")
	flag.IntVar(&port, "port", 8080, "set the server service port")
	flag.Parse()
}
func main() {
	defer glog.Flush()
	glog.Infof("the server starting ip:%s, port:%d", ip, port)
	//拼接服务地址
	serverAddr := fmt.Sprintf("%s:%d", ip, port)
	//创建Server并增加过滤器链
	server := niko.NewNikoServer("nikolas-server", niko.MetricsFilterBuilder,
		niko.LogFilterBuilder)
	//注册服务路由
	server.RegisterRoute("GET", "/getHeader", service.GetReqHeader)
	server.RegisterRoute("GET", "/healthz", service.CheckHealth)
	server.RegisterRoute("POST", "/niko/user/signup/", service.UserSignUp)
	server.RegisterRoute("POST", "/niko/user/query/id", service.QueryUserById)
	//启动Server
	err := server.Start(serverAddr)
	if err != nil {
		glog.Fatal(err)
	}
}
