# httpserver

云原生训练营作业
1. 接收客户端 request，并将 request 中带的 header 写入 response header
2. 读取当前系统的环境变量中的 VERSION 配置，并写入 response header
3. Server 端记录访问日志包括客户端 IP，HTTP 返回码，输出到 server 端的标准输出 
4. 当访问 localhost/healthz 时，应返回200

实现方式：
（第一次接触GO项目，目录结构有点混乱）
模仿极客时间大明老师的课程，写了一个简单的基于GO SDK实现的简单HTTP SEVER，通过注册过滤器实现
对访问服务的请求进行拦截，记录客户端ip和返回响应的状态码，并内置了一个内存MAP可以完成用户的注册
和查询功能等。

执行步骤
1. make build 
2. make run 
3. 在postman中执行 localhost:8080/getHeader 请求method GET 
   系统将request中的header写入response的header中，并将系统环境
   变量VERSION写入response的header中，我用的是MacBooK Pro获取不到这个系统
   环境变量，返回空。 
4. 在postman中执行 localhost:8080/healthz 请求method GET
   系统会在返回body中写入200 
5. 在postman中执行 localhost:8080/niko/user/signup 请求method POST body参数:
  {"user_id":1,"user_name":"老毕","password":"123456","phone":"15810146711",
   "identity":"130321112306028114","email_addr":"hahah_java@163.com"}可以完成用户的注册功能。
6. 在postman中执行 localhost:8080/niko/user/query/id 请求method POST body参数:
  {"user_id":1} 可以完成注册用户的查询功能。
7. 系统的每次请求都会用log记录请求的remote IP 和返回的状态码，以及请求的耗时。

