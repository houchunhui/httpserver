BUILD_NAME=httpserver
SOURCE=cmd/niko-server/main.go
VERSION=v2
REPOSITORY=houchunhui/

.PHONY: run,build,go_build,clean,push
run:build
	docker run "${REPOSITORY}""${BUILD_NAME}":"${VERSION}"
build:go_build
	docker build -t "${REPOSITORY}""${BUILD_NAME}":"${VERSION}" .
go_build:clean
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o "${BUILD_NAME}" ${SOURCE}
clean:
	go clean
	rm "${BUILD_NAME}"
push:
	docker push "${REPOSITORY}""${BUILD_NAME}":"${VERSION}"