package niko

import (
	"github.com/golang/glog"
	niko "gitlab.com/houchunhui/httpserver/v1/pkg/error"
	"strings"
)

// Handler 注册和服务路由
type Handler interface {
	Routable
	ServeRoute
}

// TreeHandler 基于树结构实现的路由注册和服务路由
type treeHandler struct {
	Node *Node
}

// FormatPattern 格式化路由字符串去掉首尾"/"后并按"/"进行切分
func (t *treeHandler) formatPattern(pattern string) []string {
	pattern = strings.Trim(pattern, "/")
	return strings.Split(pattern, "/")
}

// ServeHttp 根据用户的请求URL进行服务路由找到对应的Handler
func (t *treeHandler) ServeHttp(ctx *Context) {
	method := ctx.R.Method
	pattern := t.formatPattern(ctx.R.URL.Path)
	cur := t.Node
	for _, path := range pattern {
		node, bol := cur.FindChildNodeByPath(method, path)
		if !bol {
			err := ctx.WriteNotFound("request URL not found")
			if err != nil {
				glog.Error(err)
			}
			return
		}
		cur = node
	}
	if cur.HandleFunc == nil {
		err := ctx.WriteNotFound("request URL not found")
		if err != nil {
			glog.Error(err)
		}
		return
	}
	cur.HandleFunc(ctx)
}

// RegisterRoute 注册路由
func (t *treeHandler) RegisterRoute(method string, pattern string, handleFunc HandleFunc) error {
	err := t.validatePattern(pattern)
	if err != nil {
		return err
	}
	pattern = strings.Trim(pattern, "/")
	paths := strings.Split(pattern, "/")
	cur := t.Node
	for index, path := range paths {
		//查询path是否存在
		node, bol := cur.FindChildNodeByPath(method, path)
		if !bol {
			cur.AddChildNode(method, paths[index:], handleFunc)
			return nil
		} else {
			cur = node
		}
	}
	cur.HandleFunc = handleFunc
	return nil
}

// 校验用户注册的路由当前版本是否支持 目前只支持/*的通配符匹配
func (t *treeHandler) validatePattern(pattern string) error {
	pos := strings.Index(pattern, "*")
	if pos > 0 {
		if pos != len(pattern)-1 {
			return niko.ErrorInvalidRouterPattern
		}
		if pattern[pos-1] != '/' {
			return niko.ErrorInvalidRouterPattern
		}
	}
	return nil
}

// NewTreeHandler 创建Handler,对外屏蔽内部实现细节
func NewTreeHandler() Handler {
	return &treeHandler{
		Node: &Node{
			Pattern: "/",
		},
	}
}
