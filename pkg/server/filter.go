package niko

import (
	"github.com/golang/glog"
	"strings"
	"time"
)

// Filter 过滤器
type Filter func(ctx *Context)

// FilterBuilder filter构建器
type FilterBuilder func(next Filter) Filter

// MetricsFilterBuilder 请求耗时过滤器
func MetricsFilterBuilder(next Filter) Filter {
	return func(ctx *Context) {
		start := time.Now().Nanosecond()
		next(ctx)
		end := time.Now().Nanosecond()
		glog.Infof("%s请求耗时:%d微秒", ctx.R.URL.Path, (end-start)/1000)
	}
}

// LogFilterBuilder 记录请求日志信息,请求的IP以及返回的状态码
func LogFilterBuilder(next Filter) Filter {
	return func(ctx *Context) {
		ip := strings.Split(ctx.R.RemoteAddr, ":")[0]
		glog.Infof("%s request of remote ip is %s", ctx.R.URL.Path, ip)
		next(ctx)
		glog.Infof("%s response of http status code is %d", ctx.R.URL.Path, ctx.Code)
	}
}
