package niko

type HandleFunc = func(ctx *Context)

// Routable 注册路由
type Routable interface {
	RegisterRoute(method string, pattern string, handleFunc HandleFunc) error
}

// ServeRoute 服务路由
type ServeRoute interface {
	ServeHttp(ctx *Context)
}
