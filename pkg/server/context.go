package niko

import (
	"encoding/json"
	"io"
	"net/http"
)

// Context 一次请求的应用上下文
type Context struct {
	W    http.ResponseWriter
	R    *http.Request
	Code int //Response响应状态码
}

// NewContext 创建应用上下文
func NewContext(W http.ResponseWriter, R *http.Request) *Context {
	return &Context{
		W: W,
		R: R,
	}
}

// ReadJson 将body中的字节流转换成JSON
func (nc *Context) ReadJson(res interface{}) error {
	body, err := io.ReadAll(nc.R.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(body, res)
}

// WriteOK 成功返回JSON
func (nc *Context) WriteOK(resp interface{}) error {
	return nc.WriteJson(http.StatusOK, resp)
}

// WriteBadRequest HTTP Bad Request
func (nc *Context) WriteBadRequest(resp interface{}) error {
	return nc.WriteJson(http.StatusBadRequest, resp)
}

// WriteNotFound request not found
func (nc *Context) WriteNotFound(resp interface{}) error {
	return nc.WriteJson(http.StatusNotFound, resp)
}

// WriteJson 响应状态码并把对象序列化写进body
func (nc *Context) WriteJson(statusCode int, resp interface{}) error {
	body, err := json.Marshal(resp)
	if err != nil {
		return err
	}
	nc.W.WriteHeader(statusCode)
	nc.Code = statusCode
	_, err = nc.W.Write(body)
	return err
}
