package niko

type BaseNode interface {
	AddChildNode(method string, paths []string, handleFunc HandleFunc)
	FindChildNodeByPath(method string, pattern string) (*Node, bool)
}

//Node 路由节点
type Node struct {
	Method     string
	Pattern    string
	HandleFunc HandleFunc
	Children   []*Node
}

//AddChildNode 增加路由节点
func (n *Node) AddChildNode(method string, paths []string, handleFunc HandleFunc) {
	if len(paths) == 1 {
		n.Children = append(n.Children, &Node{
			Method:     method,
			Pattern:    paths[0],
			HandleFunc: handleFunc,
		})
		return
	}
	node := &Node{
		Method:  method,
		Pattern: paths[0],
	}
	n.Children = append(n.Children, node)
	node.AddChildNode(method, paths[1:], handleFunc)
	return
}

// FindChildNodeByPath 查找路由节点
func (n *Node) FindChildNodeByPath(method string, pattern string) (*Node, bool) {
	if n.Children == nil || len(n.Children) == 0 {
		return nil, false
	}
	var wildcardNode *Node
	for _, c := range n.Children {
		if c.Method == method && c.Pattern == pattern && pattern != "*" {
			return c, true
		}
		if c.Method == method && c.Pattern == "*" {
			wildcardNode = c
		}
	}
	return wildcardNode, wildcardNode != nil
}
