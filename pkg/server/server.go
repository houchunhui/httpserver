package niko

import (
	"net/http"
	"sync"
)

// NikolasServer niko服务接口
type NikolasServer interface {
	Routable
	Start(addr string) error
}

// 基于Http SDK实现的Niko服务
type sdkHttpNikoServer struct {
	Mutex      sync.Mutex
	ServerName string
	Handler    Handler
	Root       Filter
}

// RegisterRoute 向服务器中注册路由服务
func (s *sdkHttpNikoServer) RegisterRoute(method string, pattern string, handleFunc HandleFunc) error {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()
	return s.Handler.RegisterRoute(method, pattern, handleFunc)
}

// Start 启动niko服务
func (s *sdkHttpNikoServer) Start(addr string) error {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		c := NewContext(writer, request)
		s.Root(c)
	})
	return http.ListenAndServe(addr, nil)
}

// NewNikoServer 创建niko服务
func NewNikoServer(serverName string, builders ...FilterBuilder) NikolasServer {
	handler := NewTreeHandler()
	var root Filter = func(ctx *Context) {
		handler.ServeHttp(ctx)
	}
	for i := len(builders) - 1; i >= 0; i-- {
		b := builders[i]
		root = b(root)
	}
	return &sdkHttpNikoServer{
		ServerName: serverName,
		Handler:    handler,
		Root:       root,
	}
}
