package niko

import "errors"

// ErrorInvalidRouterPattern 服务路由不支持
var ErrorInvalidRouterPattern = errors.New("invalid router pattern")
