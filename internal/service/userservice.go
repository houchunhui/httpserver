package service

import (
	"github.com/golang/glog"
	niko "gitlab.com/houchunhui/httpserver/v1/pkg/server"
	"sync"
)

// UserMap 用户信息内存数据库
var UserMap sync.Map

type User struct {
	UserId    int32  `json:"user_id"`
	UserName  string `json:"user_name"`
	Password  string `json:"password"`
	Phone     string `json:"phone"`
	Identity  string `json:"identity"`
	EmailAddr string `json:"email_addr"`
}

// UserSignUp 用户注册
func UserSignUp(ctx *niko.Context) {
	User := &User{}
	err := ctx.ReadJson(User)
	if err != nil {
		err = ctx.WriteBadRequest("请求参数异常")
		if err != nil {
			glog.Error(err)
		}
	}
	key := User.UserId
	UserMap.Store(key, User)

	err = ctx.WriteOK("用户注册成功")
	if err != nil {
		glog.Error(err)
	}
	return
}

// QueryUserById 根据用户Id查询用户信息
func QueryUserById(ctx *niko.Context) {
	User := &User{}
	err := ctx.ReadJson(User)
	if err != nil {
		err := ctx.WriteBadRequest("请求参数异常")
		if err != nil {
			glog.Error(err)
		}
	}

	key := User.UserId
	if key == 0 {
		err := ctx.WriteBadRequest("请输入要查询的用户id")
		if err != nil {
			glog.Error(err)
		}
	}

	u, ok := UserMap.Load(key)
	if ok {
		err := ctx.WriteOK(u)
		if err != nil {
			glog.Error(err)
		}
	} else {
		err := ctx.WriteOK("用户信息不存在")
		if err != nil {
			glog.Error(err)
		}
	}
}
