package service

import (
	"github.com/golang/glog"
	niko "gitlab.com/houchunhui/httpserver/v1/pkg/server"
	"os"
)

// GetReqHeader 获取请求头信息并将其写入到响应头中
func GetReqHeader(ctx *niko.Context) {
	headers := ctx.R.Header
	if headers != nil && len(headers) > 0 {
		for key, value := range headers {
			if value != nil && len(value) > 0 {
				for _, v := range value {
					ctx.W.Header().Set(key, v)
				}
			}
		}
	}
	//获取系统变量VERSION并将其写入到响应头中
	version := os.Getenv("VERSION")
	ctx.W.Header().Set("VERSION", version)
	err := ctx.WriteOK("request header wrote to response header")
	if err != nil {
		glog.Error(err)
	}
	return
}

// CheckHealth health check
func CheckHealth(ctx *niko.Context) {
	err := ctx.WriteOK(200)
	if err != nil {
		glog.Error(err)
	}
}
