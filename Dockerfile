FROM ubuntu
COPY httpserver ./httpserver
ENTRYPOINT ["./httpserver","-alsologtostderr"]